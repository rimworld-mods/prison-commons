﻿using System;
using HarmonyLib;
using RimWorld;
using Verse;

namespace PrisonCommons
{
    [HarmonyPatch(typeof(SocialProperness), nameof(SocialProperness.IsSociallyProper), new Type[] { typeof(Thing), typeof(Pawn), typeof(bool), typeof(bool) })]
    static class SocialProperness_IsSociallyProper_Patch
    {
        public static void Postfix(ref bool __result, Thing t, Pawn p, bool forPrisoner, bool animalsCare)
        {
            if (!__result && forPrisoner && p != null)
            {
                var interactionSpot = t.def.hasInteractionCell ? t.InteractionCell : t.Position;
                var interactionRoom = interactionSpot.GetRoomOrAdjacent(t.Map);

                if (interactionRoom != null)
                {
                    __result = PrisonCommons.IsPrisonCommons(interactionRoom) || PrisonCommons.IsAllowedDoorway(p, interactionRoom);

                    if (!__result)
                    {
                        var prisonerBed = p.ownership.OwnedBed;
                        var prisonerBedroom = prisonerBed?.GetRoom();

                        __result = interactionRoom == prisonerBedroom;
                    }
                }
            }
        }
    }
}
