﻿using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace RimWorld
{
    [StaticConstructorOnStartup]
    public class CompPrisonCommons : ThingComp
    {
        private static readonly Texture2D ForPrisonersTex = ContentFinder<Texture2D>.Get("UI/Commands/ForPrisoners");

        private bool isPrisonCommons = false;

        public bool Active
        {
            get
            {
                return isPrisonCommons;
            }
            set
            {
                if (isPrisonCommons == value)
                {
                    return;
                }
                isPrisonCommons = value;
                if (parent.Spawned)
                {
                    PrisonCommons.PrisonCommons.SetPrisonCommons(this, isPrisonCommons);
                }
            }
        }

        public override void PostExposeData()
        {
            Scribe_Values.Look(ref isPrisonCommons, "isPrisonCommons", defaultValue: false);
        }

        public override void PostDeSpawn(Map map)
        {
            base.PostDeSpawn(map);
            if (Active)
            {
                PrisonCommons.PrisonCommons.SetPrisonCommons(this, false);
            }
        }

        public override void PostDestroy(DestroyMode mode, Map previousMap)
        {
            base.PostDestroy(mode, previousMap);
            if (Active)
            {
                PrisonCommons.PrisonCommons.SetPrisonCommons(this, false);
            }
        }

        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            base.PostSpawnSetup(respawningAfterLoad);
            if (parent.Faction != Faction.OfPlayer && !respawningAfterLoad)
            {
                isPrisonCommons = false;
            }
            if (Active)
            {
                PrisonCommons.PrisonCommons.SetPrisonCommons(this, true);
            }
        }

        public override IEnumerable<Gizmo> CompGetGizmosExtra()
        {
            Command_Toggle command_Toggle = new Command_Toggle();
            command_Toggle.defaultLabel = "CommandPrisonCommonsToggleLabel".Translate();
            command_Toggle.icon = ForPrisonersTex;
            command_Toggle.isActive = () => Active;
            command_Toggle.toggleAction = delegate
            {
                Active = !Active;
            };
            if (Active)
            {
                command_Toggle.defaultDesc = "CommandPrisonCommonsToggleDescActive".Translate();
            }
            else
            {
                command_Toggle.defaultDesc = "CommandPrisonCommonsToggleDescInactive".Translate();
            }
            yield return command_Toggle;
        }
    }
}
